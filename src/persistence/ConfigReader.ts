import Item from "../Item";

export default interface ConfigReader {

  get(id: number): Item;

  getByKey (key: string): Item[];

}
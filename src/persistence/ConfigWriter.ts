import Item from "../Item";

export default interface ConfigWriter {

  save(config: Item): void;

  delete(id: number): void;

  reorder(key: string, parentId: number, ids: number[]): void;

}
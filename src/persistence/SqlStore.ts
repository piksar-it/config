import ConfigWriter from './ConfigWriter'
import ConfigReader from './ConfigReader'
import Item from '../Item';

/**
 * A class that allows for messages to be stored in
 * a relational database such as Postgres or MySql
 *
 * Included here as an example and its not implemented or used.
 */
export default class SqlStore implements ConfigReader, ConfigWriter {
  
  save(config: Item): void
  {

  }

  delete(id: number): void
  {

  }

  reorder(key: string, parentId: number, ids: number[]): void
  {

  }

  read(id: number): string {
    // Read from database here
    return ''
  }


  get(id: number): Item
  {
    return {} as Item;
  }

  getByKey (key: string): Item[]
  {
    return [];
  }

}
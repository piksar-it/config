import ConfigWriter from "./ConfigWriter";
import ConfigReader from "./ConfigReader";

export default class MessageStore {
  writer: ConfigWriter;
  reader: ConfigReader;

  constructor(writer: ConfigWriter, reader: ConfigReader) {
    if(writer === null) {
      throw new Error("writer argument cannot be null")
    }
    if(reader === null) {
      throw new Error("reader argument cannot be null")
    }
    this.writer = writer;
    this.reader = reader;
  }

  /**
   *
   * @param id the id of the message to save
   * @param message the text message to write to storage
   *
   */
  public save (id: number, message: string) {
    // this.writer.save();
  }

   /**
   *
   * @param id the id of the message to read
   * @returns message string
   *
   */
  public read(id: number): string {
    // return this.reader.read(id);
    return '';
  }
}
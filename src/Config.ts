import Item from './Item';
import ConfigReader from './persistence/ConfigReader';

export default class Config {

  constructor(private _repository: ConfigReader) { }

  get(key: string, attributes: { [key: string]: any; }): { [key: string]: any; } {
      var configItems = this._repository.getByKey(key);

      return this.matchConfigs(configItems, attributes, 0);
  }

  private matchConfigs(configItems: Item[], attributes: { [key: string]: any; }, parentId: number): { [key: string]: any; } {
        var configs = {};

        for (var item of configItems) {
            if (item.parentId != parentId) {
                continue;
            }

            // console.log(item, attributes, item.isSatisfiedBy(attributes));

            if (item.isSatisfiedBy(attributes)) {
                configs = this.patch(
                    configs,
                    this.patch(
                        item.value,
                        this.matchConfigs(configItems, attributes, item.id)
                    )
                );
            }
        }

        return configs;
    }
  
    private patch(prevConfig: { [key: string]: any; }, newConfig: { [key: string]: any; }): { [key: string]: any; } {
        return {...prevConfig, ...newConfig};
    }


  // public save(id: number, message: string): void {
  //   var fileFullName = this.getFileInfo(id);
  //   fs.writeFileSync(fileFullName, message)
  // }

  // public read(id: number): string {
  //   var fileFullName = this.getFileInfo(id);
  //   var exists = fs.existsSync(fileFullName);
  //   if(!exists) {
  //     return undefined
  //   }
  //   return fs.readFileSync(fileFullName, {encoding: 'ascii'});
  // }

  // public getFileInfo(id: number): string {
  //   return path.join(__dirname, this.directory, `${id}.txt`)
  // }
}
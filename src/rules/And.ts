import Rule from "./Rule";

export default class And implements Rule {

    constructor(protected _rules: Rule[]) { }

    isSatisfiedBy(attributes: { [key: string]: any; }): boolean {
        for (var rule of this._rules) {
            if (!rule.isSatisfiedBy(attributes)) {
                return false;
            }
        }
  
        return true;
    }

    // protected function asArray(): array
    // {
    //     return ['key' => $this->key, 'values' => $this->values];
    // }
    
    // protected static function fromArray(array $data): self
    // {
    //     return new self($data['key'], $data['values']);
    // }
}
import Rule from "./Rule";

export default class In implements Rule {

    constructor(protected _key: string, protected _values: string[]) { }

    isSatisfiedBy(attributes: { [key: string]: any; }): boolean {
        // if (!attributes.hasOwnProperty(this._key)) {
        //     console.log(attributes);
        //     throw new Error(`The key "${this._key}"" has to be present`);
        // }

        return attributes.hasOwnProperty(this._key) && this._values.includes(attributes[this._key]);
    }

    // protected function asArray(): array
    // {
    //     return ['key' => $this->key, 'values' => $this->values];
    // }
    
    // protected static function fromArray(array $data): self
    // {
    //     return new self($data['key'], $data['values']);
    // }
}
import Rule from "./Rule";

export default class Not implements Rule {

    constructor(protected _rule: Rule) { }

    isSatisfiedBy(attributes: { [key: string]: any; }): boolean {
        return !this._rule.isSatisfiedBy(attributes);
    }

    // protected function asArray(): array
    // {
    //     return ['key' => $this->key, 'values' => $this->values];
    // }
    
    // protected static function fromArray(array $data): self
    // {
    //     return new self($data['key'], $data['values']);
    // }
}
export default interface Rule {
    
    isSatisfiedBy(attributes: { [key: string]: any; }): boolean;

    // protected abstract function asArray(): array;

    // protected abstract static function fromArray(array $data): self;
    
    // public function toArray(): array
    // {
    //     return array_merge(['name' => substr(strrchr(get_class($this), "\\"), 1)], $this->asArray());
    // }

    // public static function recreate(?array $data): ?self
    // {
    //     if (!$data) {
    //         return null;
    //     }

    //     $className = 'Nf\Config\Rules\\' . $data['name'];

    //     if (!class_exists($className)) {
    //         throw new \Exception(sprintf('Incorrect rule %s', $data['name']));
    //     }

    //     return $className::fromArray($data);
    // }

}
import Rule from "./rules/Rule";

export default class Item {

    constructor(
        public readonly id: number,
        public readonly parentId: number,
        public readonly key: string,
        public readonly _name: string,
        public readonly rule: Rule,
        public readonly value: { [key: string]: any; },
        public readonly isActive: boolean = true
    ) { }

    public isSatisfiedBy (attributes: any): boolean {
        return !this.rule || this.rule.isSatisfiedBy(attributes);
    }

}
import In from '../../src/rules/In';
import And from '../../src/rules/And';

describe('And', () => {
    var rule = new And([
        new In('role', ['tester']),
        new In('department', ['it'])
    ]);

    it('both rules are satisfied', () => {
        expect(rule.isSatisfiedBy({'role': 'tester', 'department': 'it'})).toBeTruthy();
    });

    it('one rule is satisfied', () => {
        expect(rule.isSatisfiedBy({'role': 'tester', 'department': 'hr'})).toBeFalsy();
        expect(rule.isSatisfiedBy({'role': 'assistant', 'department': 'it'})).toBeFalsy();
    });

    it('none of the rules are satisfied', () => {
        expect(rule.isSatisfiedBy({})).toBeFalsy();
    });
});
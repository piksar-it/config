import In from '../../src/rules/In';
import Or from '../../src/rules/Or';

describe('Or', () => {
    var rule = new Or([
        new In('role', ['tester']),
        new In('department', ['it'])
    ]);

    it('both rules are satisfied', () => {
        expect(rule.isSatisfiedBy({'role': 'tester', 'department': 'it'})).toBeTruthy();
    });

    it('one rule is satisfied', () => {
        expect(rule.isSatisfiedBy({'role': 'tester', 'department': 'hr'})).toBeTruthy();
        expect(rule.isSatisfiedBy({'role': 'assistant', 'department': 'it'})).toBeTruthy();
    });
    
    it('none of the rules are satisfied', () => {
        expect(rule.isSatisfiedBy({})).toBeFalsy();
    });
});
import In from '../../src/rules/In';
import Not from '../../src/rules/Not';

describe('Not', () => {
    var rule = new Not(
        new In('role', ['tester', 'developer'])
    );

    it('exists in data set', () => {
        expect(rule.isSatisfiedBy({'role': 'developer'})).toBeFalsy();
    });

    it('does not exist in data set', () => {
        expect(rule.isSatisfiedBy({})).toBeTruthy();
    });

    it('there is key but value does not match', () => {
        expect(rule.isSatisfiedBy({'role': 'admin'})).toBeTruthy();
    });
});
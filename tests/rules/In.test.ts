import In from '../../src/rules/In';

describe('In', () => {
    var rule = new In('role', ['tester', 'developer']);

    it('exists in data set', () => {
        expect(rule.isSatisfiedBy({'role': 'developer'})).toBeTruthy();
    });

    it('does not exist in data set', () => {
        expect(rule.isSatisfiedBy({})).toBeFalsy();
    });

    it('there is key but value does not match', () => {
        expect(rule.isSatisfiedBy({'role': 'admin'})).toBeFalsy();
    });
});
import Config from '../src/Config';
import Item from '../src/Item';
import And from '../src/rules/And';
import In from '../src/rules/In';
import Not from '../src/rules/Not';
import InMemoryConfigRepository from './InMemoryConfigRepository';

describe('Permissions config', () => {

    var reader = new InMemoryConfigRepository([
        new Item(1, 0, 'permissions', 'Tests tab view', new In('role', ['tester', 'developer']), {canSeeTests: true}),
        new Item(2, 0, 'permissions', 'Tests tab modify', 
            new And([
                new In('role', ['developer']),
                new Not(
                    new In('name', ['kamil']),
                )
            ]), {canEditTests: true}),
    ]);

    var config = new Config(reader);
    
    describe('Tester roles', () => {
        var testerRoles = config.get('permissions', {
            'role': 'tester'
        })

        it('tester can see tests tab', () => {
            expect(testerRoles['canSeeTests']).toBeTruthy();
        });

        it('tester can\'t edit tests tab', () => {
            expect(testerRoles['canEditTests']).toBeUndefined();
        });
    });

    describe('Developer roles', () => {
        var developerRoles = config.get('permissions', {
            'role': 'developer',
            'name': 'janusz',
        })

        it('developer can edit tests tab', () => {
            expect(developerRoles['canSeeTests']).toBeTruthy();
            expect(developerRoles['canEditTests']).toBeTruthy();
        });

        var developerKamilRoles = config.get('permissions', {
            'role': 'developer',
            'name': 'kamil',
        })

        it('kamil can\'t edit tests tab', () => {
            expect(developerKamilRoles['canEditTests']).toBeUndefined();
        });
    });
});
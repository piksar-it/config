import Item from '../src/Item';
import ConfigReader from '../src/persistence/ConfigReader';

export default class InMemoryConfigRepository implements ConfigReader {

    constructor(private data: Item[]) { }

    get(id: number): Item {
        return this.data.filter(item => item.id === id)[0];
    }

    getByKey (key: string): Item[] {
        return this.data.filter(item => item.key === key);
    }

}